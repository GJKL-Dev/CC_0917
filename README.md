## Unser Beitrag zur Code Competition von IT-Talents.de für den Monat September 2017

Die Aufgabe war es Conway's "Game of Life" zu entwickeln. Die Programmiersprache sowie die Umsetzung sprich Desktop-, Konsolen- oder Webanwendung waren dabei frei wählbar.

Alle weiteren Infos findet ihr unter: https://www.it-talents.de/foerderung/code-competition/code-competition-09-2017

### Installation
 
Das Spiel gibt es unter Tags -> Release-1.0 als .jar sowie als .exe zum herunterladen. Wichtig dabei ist, dass mindestens Java 8 auf dem Computer installiert ist. Windows User können auch die .exe verwenden, welche die benötigte Java Version mitliefert, somit muss nichts weiter installiert werden.

Unterfolgendem Link lässt sich die neuste Java Version herunterladen: https://java.com/de/download/

### Starten des Programmes

Wenn Java 8 auf dem ausführenden Computer installiert ist, (außer Windows mit der .exe) lässt sich das Programm mit einem einfachen Doppelklick installieren.

### Was ist das Game of Life?

Conway's Game of Life (Spiel des Lebens) ist ein zweidimensionaler zellulärer Automat, welcher 1970 von John Horton Conway entworfen wurde. Das Game of Life spielt in einer zweidimensionalen Welt, welche aus vielen einzelnen Zellen besteht. Diese Zellen können einen von zwei Zuständen besitzen, tot oder lebendig. An Hand von gewissen Regeln wird die gesamte Welt bei jedem "Lebenszyklus" in eine neue Generation übergeführt, wobei neues Leben entsteht oder Bisheriges erlischt.

##### Die Regeln:
Wichtig: Jede Zelle hat acht Nachbarfelder ( senkrecht, waagerecht und diagonal ). Falls sich eine Zelle am Rand der Welt befindet, geht es auf der gegenüberliegenden Seite weiter.

- Jede lebende Zelle mit weniger als zwei lebenden Nachbarn stirbt
- Jede lebende Zelle mit zwei oder drei lebenden Nachbarn lebt weiter
- Jede lebende Zelle mit mehr als drei lebenden Nachbarn stirbt
- Jede tote Zelle mit genau drei lebenden Nachbarn wird lebendig

### Unser Programm

Zu Beginn kann zunächst eine frei wählbare Größe des Spielfeldes ausgewählt werden. In dem gebildeten Spielfeld kann man nun per Linksklick den Status der Zellen zwischen tot oder lebendig einstellen. Alternativ kann man auch den Random-Button betätigen um eine zufällige Startgeneration zu generieren. Hat man nun sein Spielfeld soweit eingestellt, kann man über den Start-Button das Spektakel seinen Lauf lassen. Anhand des Spinners kann man die Spielgeschwindigkeit verändern und über die Navigationsknöpfe den Zyklus anhalten und sogar einzelne Generationen vor oder zurück springen. \
Neben dem Reset-Knopf, welcher das Spielfeld auf die ursprüngliche Generation zurücksetzt, kann man auch das ganze Spielfeld auf tot setzen.

### Verwendete Technologien

Für unsere Anwendung haben wir Java 8 verwendet und als einziges Framework JavaFX für unsere GUI verwendet.

### Unsere Features

Man kann manuell vor und zurück gehen, sowie das Feld wieder löschen (Alle Felder auf tot). \
Außerdem kann man das Spielfeld als Datei abspeichern und später wieder öffnen.