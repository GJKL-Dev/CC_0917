package de.gjkl.gui;

import de.gjkl.entities.Field;
import de.gjkl.game.Game;
import de.gjkl.utilities.FileManager;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.File;

/**
 * Is the main application of the entire program.
 *
 * @author Gerrit Koehler
 */
public class MainFrame extends Application implements Runnable {

    private Stage window;
    private BorderPane borderPane = new BorderPane();
    private BorderPane mainBorderPane = new BorderPane();

    private FileManager fileManager = new FileManager();
    private File location;

    private HBox topHBox = new HBox();
    private HBox bottomHBox = new HBox();

    private Canvas canvas = new Canvas();
    private GraphicsContext graphicsContext;

    private Field hoverField = null;
    private double mouseX, mouseY;

    private int oldX, oldY;
    private boolean dragged = false;
    private boolean touch = false;

    private Timeline timeline;

    private FileChooser fileChooser = new FileChooser();

    private static int resolution;
    public static int getResolution() {return resolution;}
    private static final int  FONTSIZE = 20;
    private static final Font COMMONFONT = new  Font("Devanagari MT", FONTSIZE);

    @Override
    public void start(Stage primaryStage) throws Exception {

        window = primaryStage;

        window.setHeight(600);
        window.setWidth(1000);
        window.setMinHeight(600);
        window.setMinWidth(1000);
        window.setTitle("The Game of Life");

        window.widthProperty().addListener(e -> {
            topHBox   .setSpacing((window.getWidth() - 800) / 6 + 13);
            bottomHBox.setSpacing((window.getWidth() - 1000) / 9 + 5);
        });

        fileChooser.getExtensionFilters().add(
                new FileChooser.ExtensionFilter(
                        "The Game of Life Files (.cgol)",
                        "*.cgol"));

        //Top
        Label resolutionLabel = new Label("Resolution:");
        Slider resolutionSlider = new Slider(10, 50, 20);
        Label widthLabel = new Label("Width:");
        Spinner<Integer> widthSpinner = new Spinner<>();
        Label heightLabel = new Label("Height:");
        Spinner<Integer> heightSpinner = new Spinner<>();
        Button refreshButton = new Button("Refresh");

        //Bottom
        Button startButton = new Button("Start");
        Button pauseButton  = new Button("Pause");
        Button backButton  = new Button("Back");
        Button forthButton  = new Button("Forth");
        Button resetButton = new Button("Reset");
        Button clearButton = new Button("Clear");
        Button randomButton = new Button("Random");
        Label speedLabel = new Label("Speed:");
        Spinner<Integer> speedSpinner = new Spinner<>();
        Label currentGenerationLabel = new Label("Generation:");
        Label currentGenerationNumber = new Label("" + Game.getInstance().getFieldController().getGeneration());

        //Menus
        final Menu fileMenu = new Menu("_File");

        MenuItem newMenuItem      = new MenuItem("_New");
        MenuItem openFileMenuItem = new MenuItem("_Open File...");
        MenuItem saveMenuItem     = new MenuItem("_Save");
        MenuItem saveAsMenuItem   = new MenuItem("Save _As...");

        MenuBar menuBar = new MenuBar();

        //Top
        resolutionLabel.setFont(COMMONFONT);
        resolutionLabel.setPadding(new Insets(7,0,0,10));
        resolutionLabel.setPrefWidth(110);
        resolutionLabel.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);

        resolutionSlider.setStyle("-fx-font-family: Devanagari MT;-fx-font-size: 20;");
        resolutionSlider.setPrefWidth(100);
        resolutionSlider.setPadding(new Insets(17,0,0,0));
        resolutionSlider.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);

        resolutionSlider.setOnMouseDragReleased(e -> {
            resolution = ((int)(resolutionSlider.getValue()));
            rePaint();
        });

        widthLabel.setFont(COMMONFONT);
        widthLabel.setPadding(new Insets(7,0,0,0));
        widthLabel.setPrefWidth(60);
        widthLabel.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);

        SpinnerValueFactory svfW = new SpinnerValueFactory
                .IntegerSpinnerValueFactory(1, Integer.MAX_VALUE);
        svfW.setValue(50);

        widthSpinner.setValueFactory(svfW);
        widthSpinner.setStyle("-fx--family: Devanagari MT;-fx-font-size: 20;");
        widthSpinner.setPrefWidth(100);
        widthSpinner.setEditable(true);
        widthSpinner.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);

        widthSpinner.valueProperty().addListener(e -> {
            Game.getInstance().getFieldController().setField(new Field(widthSpinner.getValue(), heightSpinner.getValue()));
            rePaint();
        });

        heightLabel.setFont(COMMONFONT);
        heightLabel.setPadding(new Insets(7,0,0,0));
        heightLabel.setPrefWidth(65);
        heightLabel.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);

        SpinnerValueFactory svfH = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, Integer.MAX_VALUE);
        svfH.setValue(25);

        heightSpinner.setValueFactory(svfH);
        heightSpinner.setStyle("-fx--family: Devanagari MT;-fx-font-size: 20;");
        heightSpinner.setPrefWidth(100);
        heightSpinner.setEditable(true);
        heightSpinner.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);

        heightSpinner.valueProperty().addListener(e -> {
            Game.getInstance().getFieldController().setField(new Field(widthSpinner.getValue(), heightSpinner.getValue()));
            rePaint();
        });

        refreshButton.setFont(COMMONFONT);
        refreshButton.setPadding(new Insets(7));
        refreshButton.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);

        refreshButton.setOnAction(e -> {
            Game.getInstance().getFieldController().getField().setWidth(widthSpinner.getValue());
            Game.getInstance().getFieldController().getField().setHeight(heightSpinner.getValue());
            resolution = ((int)resolutionSlider.getValue());
            rePaint();
        });
        //TODO: HBox fuer jedes Label Component Paar
        topHBox.setSpacing(13);
        topHBox.setPadding(new Insets(0, 10, 10, 10));
        topHBox.getChildren().addAll(
                resolutionLabel,
                resolutionSlider,
                widthLabel,
                widthSpinner,
                heightLabel,
                heightSpinner,
                refreshButton);

        //Bottom
        startButton.setFont(COMMONFONT);
        startButton.setPadding(new Insets(10));
        startButton.setStyle("-fx-base: #00ff00;");
        startButton.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);

        startButton.setOnAction(e -> {
            fileMenu.setDisable(true);
            startButton          .setDisable(true);
            backButton           .setDisable(true);
            forthButton           .setDisable(true);
            pauseButton          .setDisable(false);
            randomButton         .setDisable(true);
            resolutionSlider     .setDisable(true);
            widthSpinner         .setDisable(true);
            heightSpinner        .setDisable(true);
            speedSpinner           .setDisable(true);
            refreshButton        .setDisable(true);


            timeline = new Timeline();
            timeline.setCycleCount(Timeline.INDEFINITE);
            timeline.getKeyFrames().add(
                    new KeyFrame(Duration.millis(
                            1000 / speedSpinner.getValue()), e1 -> {
                        currentGenerationNumber.setText("" + Game.getInstance().getFieldController().getGeneration());
                        Game.getInstance().update();
                        rePaint();
                    })
            );
            timeline.playFromStart();
        });

        pauseButton.setFont(COMMONFONT);
        pauseButton.setPadding(new Insets(10));
        pauseButton.setStyle("-fx-base: #ff0000;");
        pauseButton.setDisable(true);
        pauseButton.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);

        pauseButton.setOnAction(e -> {
            fileMenu.setDisable(false);
            pauseButton           .setDisable(true);
            startButton          .setDisable(false);
            backButton            .setDisable(false);
            forthButton           .setDisable(false);
            randomButton          .setDisable(false);
            resolutionSlider     .setDisable(false);
            widthSpinner         .setDisable(false);
            heightSpinner        .setDisable(false);
            speedSpinner           .setDisable(false);
            refreshButton        .setDisable(false);

            timeline.stop();
        });

        backButton.setFont(COMMONFONT);
        backButton.setPadding(new Insets(10));
        backButton.setStyle("-fx-base: #0000ff;");
        backButton.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);

        backButton.setOnAction(e -> {
            Game.getInstance().getFieldController().backwardStep();
            currentGenerationNumber.setText("" + Game.getInstance().getFieldController().getGeneration());
            rePaint();
        });

        forthButton.setFont(COMMONFONT);
        forthButton.setPadding(new Insets(10));
        forthButton.setStyle("-fx-base: #0000ff;");
        forthButton.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);

        forthButton.setOnAction(e -> {
            Game.getInstance().getFieldController().update();
            currentGenerationNumber.setText("" + Game.getInstance().getFieldController().getGeneration());
            rePaint();
        });

        resetButton.setFont(COMMONFONT);
        resetButton.setPadding(new Insets(10));
        resetButton.setStyle("-fx-base: #0000aa;");
        refreshButton.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);

        resetButton.setOnAction(e -> {
            timeline.stop();
            Game.getInstance().getFieldController().reset();
            Game.getInstance().getFieldController().setGeneration(0);
            currentGenerationNumber.setText("" + Game.getInstance().getFieldController().getGeneration());

            rePaint();
        });

        clearButton.setFont(COMMONFONT);
        clearButton.setPadding(new Insets(10));
        clearButton.setStyle("-fx-base: #0000aa;");
        clearButton.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);

        clearButton.setOnAction(e -> {
            timeline.stop();
            location = null;
            Game.getInstance().getFieldController().clear();
            Game.getInstance().getFieldController().setGeneration(0);
            currentGenerationNumber.setText("" + Game.getInstance().getFieldController().getGeneration());

            rePaint();
        });

        randomButton.setFont(COMMONFONT);
        randomButton.setPadding(new Insets(10));
        randomButton.setStyle("-fx-base: #0000aa;");
        randomButton.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);

        randomButton.setOnAction(e -> {
            Game.getInstance().getFieldController().clear();
            Game.getInstance().getFieldController().random();
            currentGenerationNumber.setText("" + Game.getInstance().getFieldController().getGeneration());

            rePaint();
        });

        speedLabel.setFont(COMMONFONT);
        speedLabel.setPadding(new Insets(10, -5, 0, 10));
        speedLabel.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);

        SpinnerValueFactory svfFPS = new SpinnerValueFactory
                .IntegerSpinnerValueFactory(1, Integer.MAX_VALUE);
        svfFPS.setValue(5);

        speedSpinner.setValueFactory(svfFPS);
        speedSpinner.setStyle("-fx-font-family: Devanagari MT;-fx-font-size: 20;");
        speedSpinner.setPrefWidth(100);
        speedSpinner.setEditable(true);
        speedSpinner.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);

        currentGenerationLabel.setFont(COMMONFONT);
        currentGenerationLabel.setPadding(new Insets(10, -5, 0, 10));
        currentGenerationLabel.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);

        currentGenerationNumber.setFont(COMMONFONT);
        currentGenerationNumber.setPadding(new Insets(7, 0, 0, 10));
        currentGenerationNumber.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);

        bottomHBox.setSpacing(38);
        bottomHBox.setPadding(new Insets(10));
        bottomHBox.getChildren().addAll(
                startButton,
                pauseButton,
                backButton,
                forthButton,
                resetButton,
                clearButton,
                randomButton,
                speedLabel,
                speedSpinner,
                currentGenerationLabel,
                currentGenerationNumber);

        //Center
        graphicsContext = canvas.getGraphicsContext2D();
        rePaint();

        canvas.setOnMouseMoved(e -> {
            if (hoverField != null) {
                mouseX = e.getX();
                mouseY = e.getY();

                rePaint();
            }
        });

        canvas.setOnMouseClicked(e -> {
            if (hoverField != null && touch == false) {
                if (e.getButton().toString().equals("PRIMARY")) {
                    Game.getInstance().getFieldController().insert((int) (e.getX() + mouseX / resolution),
                            (int) (e.getY() + mouseY / resolution), hoverField);
                }
                else if (e.getButton().toString().equals("SECONDARY"))
                    hoverField.rotate();
                else
                    hoverField.flip();

                rePaint();
            }
            else if (hoverField != null && touch == true) //
                System.out.println("Test");
            else{
                if (e.getButton().toString().equals("PRIMARY")) {
                    if (!dragged){
                        int x = (int) (e.getX() / resolution);
                        int y = (int) (e.getY() / resolution);

                        Game.getInstance().getFieldController().triggerCell(x, y);
                    }
                    dragged = false;
                    rePaint();
                }
            }
        });

        canvas.setOnMouseDragged(e -> {
            if (e.getButton().toString().equals("PRIMARY") && hoverField == null) {
                dragged = true;

                int x = (int) (e.getX() / resolution);
                int y = (int) (e.getY() / resolution);

                if (x != oldX || y != oldY){
                    Game.getInstance().getFieldController().triggerCell(x, y);
                }

                oldX = x;
                oldY = y;
                rePaint();
            }

            else if (hoverField != null) {
                if (e.getX() != mouseX && e.getY() != mouseY) {
                    mouseX = e.getX();
                    mouseY = e.getY();
                    rePaint();
                }
            }
        });

        canvas.setOnTouchPressed(e -> {
            System.out.println("Touch pressed");
            touch = true;
        });

        canvas.setOnTouchMoved(e -> {
            if (hoverField != null) {
                System.out.println("Touch moved" + touch);
                rePaint();
            }
        });

        canvas.setOnTouchReleased(e -> {
            touch = false;
            System.out.println("Touch releaced");
        });

        //Menu Bar
        newMenuItem.setAccelerator(
                new KeyCodeCombination(
                        KeyCode.N, KeyCombination.CONTROL_DOWN));

        newMenuItem.setOnAction(e -> {
            location = null;
            Game.getInstance().getFieldController().clear();
            Game.getInstance().getFieldController().setGeneration(0);
            currentGenerationNumber.setText("" + Game.getInstance().getFieldController().getGeneration());

            rePaint();
        });

        openFileMenuItem.setOnAction(e -> {
            fileChooser.setTitle("Open Resource File");

            location = fileChooser.showOpenDialog(window);

            if (location != null) {
                fileManager.setLocation(location.toPath());
                Game.getInstance().getFieldController().setField(fileManager.openFile());
                rePaint();

                svfW.setValue(Game.getInstance().getFieldController().getField().getWidth());
                svfH.setValue(Game.getInstance().getFieldController().getField().getHeight());
                currentGenerationNumber.setText("" + Game.getInstance().getFieldController().getGeneration());
            }
        });

        saveMenuItem.setAccelerator(
                new KeyCodeCombination(
                        KeyCode.S, KeyCombination.CONTROL_DOWN));

        saveMenuItem.setOnAction(e -> {
            if (location != null) {
                fileManager.setLocation(location.toPath());
                fileManager.saveFile(Game.getInstance().getFieldController().getField());
            }
            else {
                fileChooser.setTitle("Save as");

                location = fileChooser.showSaveDialog(window);

                if (location != null) {
                    fileManager.setLocation(location.toPath());
                    fileManager.saveFile(Game.getInstance().getFieldController().getField());
                }
            }
        });

        saveAsMenuItem.setOnAction(e -> {
            fileChooser.setTitle("Save as");

            location = fileChooser.showSaveDialog(window);

            if (location != null) {
                fileManager.setLocation(location.toPath());
                fileManager.saveFile(Game.getInstance().getFieldController().getField());
            }
        });

        fileMenu.getItems().addAll(
                newMenuItem,
                openFileMenuItem,
                new SeparatorMenuItem(),
                saveMenuItem,
                saveAsMenuItem);

        menuBar.setStyle("-fx-background-color: Whitesmoke;-fx-font-family: Devanagari MT;-fx-font-size: 16;");

        menuBar.getMenus().addAll(
                fileMenu);

        mainBorderPane.setTop(topHBox);
        mainBorderPane.setCenter(canvas);
        mainBorderPane.setBottom(bottomHBox);

        borderPane.setTop(menuBar);
        borderPane.setCenter(mainBorderPane);

        Scene scene = new Scene(borderPane);
        window.setScene(scene);
        window.show();

        refreshButton.fire();
    }

    /**
     * Re-draws field on canvas.
     */
    private void rePaint() {
        int resolution = this.resolution;

        canvas.setWidth (Game.getInstance().getFieldController().getField().getWidth()  * resolution);
        canvas.setHeight(Game.getInstance().getFieldController().getField().getHeight() * resolution);
        window.setMinWidth (Game.getInstance().getFieldController().getField().getWidth() * resolution + 100);
        window.setMinHeight(Game.getInstance().getFieldController().getField().getHeight() * resolution + 200);

        graphicsContext.setFill(Color.WHITESMOKE);
        graphicsContext.fillRect(0, 0, Game.getInstance().getFieldController().getField().getWidth() * resolution, Game.getInstance().getFieldController().getField().getHeight() * resolution);

        //Cells
        graphicsContext.setFill(Color.BLACK);
        for (int y = 0; y < Game.getInstance().getFieldController().getField().getHeight(); y++){
            for (int x = 0; x < Game.getInstance().getFieldController().getField().getWidth(); x++){
                if (Game.getInstance().getFieldController().getField().getCells()[y][x].isAlive())
                    graphicsContext.fillRect(
                            x * resolution,
                            y * resolution,
                            resolution,
                            resolution);
            }
        }

        //Grid
        graphicsContext.setFill(Color.LIGHTGREY);

        for (int i = 1; i < Game.getInstance().getFieldController().getField().getWidth(); i++)
            graphicsContext.fillRect(
                    i * resolution - .5,
                    0,
                    1,
                    Game.getInstance().getFieldController().getField().getHeight() * resolution);

        for (int i = 1; i < Game.getInstance().getFieldController().getField().getHeight(); i++)
            graphicsContext.fillRect(
                    0,
                    i * resolution - .5,
                    Game.getInstance().getFieldController().getField().getWidth() * resolution,
                    1);

        //Border
        graphicsContext.fillRect(0, 0, 2, canvas.getHeight());
        graphicsContext.fillRect(0, 0, canvas.getWidth(), 2);
        graphicsContext.fillRect(0, canvas.getHeight() - 2, canvas.getWidth(), 2);
        graphicsContext.fillRect(canvas.getWidth() - 2, 0, 2, canvas.getHeight());
    }

    @Override
    public void run() {
        launch();
    }
}