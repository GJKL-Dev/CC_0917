package de.gjkl.utilities;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import de.gjkl.entities.Field;

public class FileManager {

    private Path location;
    private Path insertLocation;

    public FileManager() {
        location = Paths.get("");
        insertLocation = Paths.get("");
    }

    public void locateInsert() {

        String os = System.getProperty("os.name").toLowerCase();

        if (os.contains("win"))
            insertLocation = Paths.get(System.getenv("APPDATA") + "\\thegameoflife");

        else if (os.contains("mac"))
            insertLocation = Paths.get(System.getProperty("user.home") + "/Library/Application Support/The Game of Life");

        else if (os.contains("nix") || os.contains("nux") || os.contains("aix"))
            insertLocation = Paths.get(System.getProperty("user.home") + "/gameoflife");

        else if (os.contains("sunos"))
            System.out.println("not sure");
    }

    public Field openFile() {

        String file = location.toString();
        try{
            @SuppressWarnings("resource")
            ObjectInputStream in = new ObjectInputStream(
                    new FileInputStream(file));
            return (Field) in.readObject();
        }catch(IOException ex){
            ex.printStackTrace();
        }catch(ClassNotFoundException ex){
            ex.printStackTrace();
        }
        return null;
    }

    public void saveFile(Field field) {

        String file = location.toString();
        try{
            ObjectOutputStream out = new ObjectOutputStream(
                    new FileOutputStream(file));
            out.writeObject(field);
            out.close();
        }catch(IOException ex){
            ex.printStackTrace();
        }
    }

    public Path getLocation() {
        return location;
    }

    public void setLocation(Path location) {
        this.location = location;
    }
}

