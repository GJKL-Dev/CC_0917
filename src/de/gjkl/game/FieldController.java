package de.gjkl.game;

import de.gjkl.entities.Cell;
import de.gjkl.entities.Field;

import java.util.Stack;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @Author Jann Luellmann
 * Controller for the field logic
 */
public class FieldController {

    private Stack<Field> fieldHistory;

    private Field field;

    private int generation;

    public FieldController() {
        
        fieldHistory = new Stack<>();
        field = new Field(50, 25);

        generation = 0;
    }

    public FieldController(int width, int height) {

        fieldHistory = new Stack<>();
        field = new Field(width, height);

        generation = 0;
    }
    
    /**
     * Updates the field to the next generation
     * 1. Copys the current gameField to a temporary array
     * 2. Checks for outer border and determines coordinates for the neighbours
     * 3. Counts neighbours
     * 4. Triggers events on the temporary array according to the amount of neighbours
     * 5. Copys the temporary array back to the gameField
     */
    public void update() {

        // Save the first generation
        if(generation == 0) {
            fieldHistory.push(new Field(field));
        }

        generation++;

        // Copy field values to the temp field
        Field temp = new Field(field);

        int neighbours;
        int upperRow, lowerRow;
        int leftCol, rightCol;

        for( int y = 0; y < temp.getHeight(); y++ ){

            for( int x = 0; x < temp.getWidth(); x++ ){

                neighbours = 0;

                // Check if the cell is at an outer border of the gameField
                // and therefore needs to check the cell on the opposite side
                upperRow = (y - 1 < 0) ? temp.getHeight()-1 : y-1;
                lowerRow = (y + 1 > temp.getHeight()-1) ? 0 : y+1;

                leftCol  = (x - 1 < 0) ? temp.getWidth()-1 : x-1;
                rightCol = (x + 1 > temp.getWidth()-1) ? 0 : x+1;

                // Check the three neighbours above
                if( field.getCells()[upperRow][leftCol].isAlive() )
                    neighbours++;
                if( field.getCells()[upperRow][x].isAlive() )
                    neighbours++;
                if( field.getCells()[upperRow][rightCol].isAlive() )
                    neighbours++;

                // Check the two neighbours in the same row
                if( field.getCells()[y][leftCol].isAlive() )
                    neighbours++;
                if( field.getCells()[y][rightCol].isAlive() )
                    neighbours++;

                // Check the three neighbours below
                if( field.getCells()[lowerRow][leftCol].isAlive() )
                    neighbours++;
                if( field.getCells()[lowerRow][x].isAlive() )
                    neighbours++;
                if( field.getCells()[lowerRow][rightCol].isAlive() )
                    neighbours++;

                // Trigger events according to amount of neighbours
                if(neighbours < 2)
                    temp.getCells()[y][x].kill();

                if(neighbours == 3)
                    temp.getCells()[y][x].revivie();

                if(neighbours > 3)
                    temp.getCells()[y][x].kill();
            }
        }

        // Copy the temp values back to the field array
        field = new Field(temp);

        // Add the new generation to the history;
        fieldHistory.push(new Field(field));
    }

    /**
     * Sets all cells to dead
     */
    public void clear() {

        for( int y = 0; y < field.getHeight(); y++ ){

            for( int x = 0; x < field.getWidth(); x++ ){

                field.getCells()[y][x].kill();
            }
        }
        fieldHistory = new Stack<>();
        generation = 0;
    }

    /**
     * Resets the gameField to the fieldHistory.get(0) (the users start setup)
     */
    public void reset() {

        if(fieldHistory.size() > 0)
            field = new Field(fieldHistory.get(0));
    }

    /**
     * Sets random cells to alive
     */
    public void random() {

        int min = Math.max(1, Math.round( (field.getWidth()*field.getHeight()) / 10 ));
        int max = Math.max(min, Math.round( (field.getWidth()*field.getHeight()) / 2 ));
        int amount = ThreadLocalRandom.current().nextInt(min, max + 1);

        int x, y;
        for( int i = 0; i < amount; i++ ){

            x = ThreadLocalRandom.current().nextInt(0, field.getWidth()-1);
            y = ThreadLocalRandom.current().nextInt(0, field.getHeight()-1);
            field.getCells()[y][x].revivie();
        }
        fieldHistory.push(field);
    }

    /**
     * Inserts a field into the current one.
     * This is used for inserting the templates.
     * @param x left coordinate of the template
     * @param y top coordinate of the template
     * @param insertField template that gets inserted
     */
    public void insert(int x, int y, Field insertField){

        int insertWidth = x + insertField.getWidth()-1 > field.getWidth() ? field.getWidth()-1 : x + insertField.getWidth()-2;
        int insertHeight = y + insertField.getHeight()-1 > field.getHeight() ? field.getHeight()-1 : y + insertField.getHeight()-2;

        int mX = x;
        int mY = y;

        //System.out.println(insertWidth + " / " + insertHeight);

        for( int insertY = 0; insertY < insertHeight; insertY++){

            for(int insertX = 0; insertX < insertWidth; insertX++){

                //TODO: Fix this    System.out.println(insertX + " / " + insertY + " -- " +mX + " / " + mY);

                field.getCells()[mY][mX].setStatus(
                        insertField.getCells()[insertY][insertX].isAlive()
                );

                mX++;
            }
            mX = x;
            mY++;
        }
    }

    /**
     * This switches the cell in the given coordinates
     * @param x x coordinate
     * @param y y coordinate
     */
    public void triggerCell(int x, int y){

        Cell cell = field.getCells()[y][x];

        if(cell.isAlive()){
            cell.kill();
        }
        else {
            cell.revivie();
        }
    }

    /**
     * Sets the current field one generation back
     */
    public void backwardStep(){

        if(fieldHistory.size() > 0)
            field = fieldHistory.pop();
    }

    /**
     * Getter and setter below this
     */

    public int getGeneration() {
        return generation;
    }

    public void setGeneration(int generation) {
        this.generation = generation;
    }

    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;
    }
}
