package de.gjkl.game;

import de.gjkl.utilities.FileManager;

/**
 * @Author Jann Luellmann
 * This class contains the game loop which updates and renders the game.
 */
public class Game {

    private static volatile Game INSTANCE = null;

    private FieldController fieldController;

    private FileManager fileManager;

    private Game() {

        fieldController = new FieldController();
        fileManager = new FileManager();
    }

    /**
     * Updates the field
     */
    public void update(){

        fieldController.update();
    }

    public static Game getInstance(){
        if( INSTANCE == null) {
            INSTANCE = new Game();
        }
        return INSTANCE;
    }

    public FieldController getFieldController() {
        return fieldController;
    }

    public void setFieldController(FieldController fieldController) {
        this.fieldController = fieldController;
    }

    public FileManager getFileManager() {
        return fileManager;
    }
}
