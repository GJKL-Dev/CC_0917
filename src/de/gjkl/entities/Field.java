package de.gjkl.entities;

import java.io.Serializable;

/**
 * @Author Jann Luellmann
 * This class represents the cells which holds all the cells
 */
public class Field implements Serializable {

    private static final long serialVersionUID = -1723253648442129097L;

    private String name;
    private int width, height;
    private Cell[][] cells;

    /**
     * Creates the 2D array with the given width and height
     * @param width Width of the game cells
     * @param height Height of the game cells
     */
    public Field(int width, int height){

        this.width = width;
        this.height = height;

        cells = new Cell[height][width];

        for(int y = 0; y < height; y++){

            for(int x = 0; x < width; x++){

                cells[y][x] = new Cell();
            }
        }
    }

    /**
     * Creates a new cells instance identical to the give one
     * @param field Field that will be copied
     */
    public Field(Field field){

        this.width = field.getWidth();
        this.height = field.getHeight();

        this.cells = new Cell[height][width];

        for(int y = 0; y < height; y++){

            for(int x = 0; x < width; x++){

                this.cells[y][x] = new Cell();
                this.cells[y][x].setStatus( field.getCells()[y][x].isAlive());
            }
        }
    }

    /**
     * Mirrors the field
     */
    public void flip(){

        Cell temp;

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width / 2; x++) {
                temp = cells[y][x];
                cells[y][x] = cells[y][height - 1 - x];
                cells[y][height - 1 - x] = temp;
            }
        }
    }

    /**
     * Rotates the field by 90 degrees
     */
    public void rotate(){

        Field rotated = new Field(height, width);

        for(int y = 0; y < height; y++){

            for(int x = 0; x < width; x++){

                rotated.cells[x][height - 1 - y] = this.cells[y][x];
            }
        }

        this.cells = rotated.cells;
    }

    /**
     * Checks if all field are empty (DEAD)
     * @return true if all fields are dead
     */
    public boolean isEmpty(){

        for(int y = 0; y < height; y++){

            for(int x = 0; x < width; x++){

                if(cells[y][x].isAlive())
                    return false;
            }
        }

        return true;
    }

    /**
     * Getter and Setter below here
     */

    public Cell[][] getCells() {
        return cells;
    }

    public void setCells(Cell[][] cells) {
        this.cells = cells;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
