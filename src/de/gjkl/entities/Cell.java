package de.gjkl.entities;

import java.io.Serializable;

/**
 * @Author Jann Luellmann
 * This class represents one single Cell in the Field. It can be dead or alive.
 * The code should be pretty self-explanatory, therefore obvious comments are avoided
 */
public class Cell implements Serializable {

    private static final long serialVersionUID = -5663318710051924159L;

    /**
     * Sets ALIVE and DEAD variables for readability
     */
    private static final boolean ALIVE = true;
    private static final boolean DEAD = false;

    private boolean status;

    public Cell(){
        status = false;
    }

    public void kill(){
        status = DEAD;
    }

    public void revivie(){
        status = ALIVE;
    }

    /**
     * Code conventions suggest 'isStatus()' as the getter, but 'isAlive' is more fitting
     */
    public boolean isAlive(){
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
